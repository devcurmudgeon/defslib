import os
from fs.osfs import OSFS
import yaml
import json

sdir = os.path.join(os.path.dirname(__file__), 'defslib/spec/schemas')
sfs = OSFS(sdir)

for x in sfs.walkfiles(wildcard='*.json-schema'):
    with sfs.open(x, "r+") as f:
        a = yaml.safe_load(f.read())
        f.seek(0)
        f.write(json.dumps(a))
        f.truncate()
