from defslib import MorphologyResolver
import os
import json

resolver = MorphologyResolver('definitions')

system = resolver.lookup('systems/base-system-x86_64-generic.morph')

with open(os.path.join(os.path.dirname(__file__), "foo.json"), "w+") as f:
    f.write(json.dumps(system, indent=4, separators=(',', ': ')))
