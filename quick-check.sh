git clone git@gitlab.com:baserock/definitions
cd definitions
git checkout lc/010
cd ..


# py3 required to run, check python and python3 commands
hash python3
PY3_BINARY_CODE=$?
PYV=$(python -c 'import sys; print(sys.version_info[0])')

if [ $PY3_BINARY_CODE == 0 ]; then
    python3 jsonize_schema.py
    python3 quick-check.py
elif [ $PYV == 3 ]; then
    python jsonize_schema.py
    python quick-check.py
else
    echo "Python3 required to use this tool."
fi
