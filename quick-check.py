from defslib import MorphologyResolver, Actuator
from copy import deepcopy
import json

aliases = {'upstream:': 'git://git.baserock.org/delta/',
           'baserock:': 'git://git.baserock.org/baserock/'}

directories = {'gits': 'gits', 'artifacts': 'artifacts', 'tmp': 'tmp'}

resolver = MorphologyResolver('definitions')

system = resolver.lookup('systems/base-system-x86_64-generic.morph')

with open("foo.json", "w+") as f:
    f.write(json.dumps(system, indent=4, separators=(',', ': ')))

resolver.validate_assemblage(system)

actuator = Actuator(directories, aliases, resolver.defaults)

working = deepcopy(system)

actuator.cache_enrich_assemblage(working)

for x in actuator.get_iterator(working):
    print('name: %s' % x['focus']['name'])
    print('supports: %s' % [a['name'] for a in x['supports']])
print(working['cache'])

flatten = deepcopy(working)

actuator.flatten_assemblage(flatten)

with open("a.json", "w+") as f:
    f.write(json.dumps(flatten, indent=4, separators=(',', ': ')))

actuator.build_assemblage(working)
