# Defslib

## Baserock Definitions Utility Library

This is a baserock definitions library, it is designed to make things easier
and more homogenous across things that use baserock definitions with python.
This library is currently V10 and python3 only. 

This library is also available on pypi here:

https://pypi.python.org/pypi/defslib

You can install it with pip install defslib

## Migrating Definitions 

You should update your definitions as follows

    git clone --recursive https://gitlab.com/baserock/defslib.git
    cd definitions
    ../defslib/defslib/spec/migrations/009-build-depends-to-system.py 
    ../defslib/defslib/spec/migrations/010-assemblages.py 

## Example Use

    from defslib import MorphologyResolver, Actuator
    from copy import deepcopy

    aliases = { 'upstream:': 'git://git.baserock.org/delta/',
                'baserock:' : 'git://git.baserock.org/baserock/' }

    directories = { 'gits': 'gits',
                    'artifacts': 'artifacts',
                    'tmp': 'tmp' }

    resolver = MorphologyResolver('definitions')

    system = resolver.lookup('systems/devel-system-x86_64-generic.morph')

    resolver.validate_assemblage(system)

    actuator = Actuator(directories, aliases, resolver.defaults)

    ## Actuator functions will mutate assemblages in place, use deep copies to
    ## preserve the originally parsed system.

    wsystem = deepcopy(system)

    ## Enrich an assemblage with cache keys for itself and recursively for all
    ## contents elements.

    actuator.cache_enrich_assemblage(wsystem)

    ## Flatten an assemblage to chunks only, preserving necessary dependency
    ## information.

    actuator.flatten_assemblage(wsystem)

## Notes

Morph files allow for dictionaries that self-update via evaluating a 'morph:'
field if found and then discarding the 'morph:' field from the dictionary.
Consequently there is no effective way to type check an individual morph file.
The MorphologyResolver quickly resolves all 'morph:' fields it finds recursively
when using either 'lookup(filename)' or 'evaluate_all()', after which the
resulting python object can be type-checked via 'validate_assemblage()'. This
resulting assemblage is completely traversable through contents and
build-depends.

For more information, see the assemblage and chunk schemas:

* https://gitlab.com/baserock/spec/blob/lc/010/schemas/assemblage.json-schema

* https://gitlab.com/baserock/spec/blob/lc/010/schemas/chunk.json-schema
